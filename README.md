Symblog
====

This is a Symfony project made with the v2.7.
It follows the tutorial [here](http://keiruaprod.fr/symblog-fr/) (Be careful, the tutorial is not adapted for Symfony 2.7 even though I decide to use 2.7 myself)

There is no other purpose but learn to use Symfony2.

Big thanks to [Clément](http://keiruaprod.fr/) who is the creator of the tutorial !
