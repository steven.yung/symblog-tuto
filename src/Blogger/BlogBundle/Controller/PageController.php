<?php

namespace Blogger\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Blogger\BlogBundle\Entity\Enquiry;
use Blogger\BlogBundle\Form\EnquiryType;

class PageController extends Controller
{
    public function indexAction()
    {
        return $this->render('BloggerBlogBundle:Page:index.html.twig', array(
            //...
        ));
    }

    public function aboutAction()
    {
        return $this->render('BloggerBlogBundle:Page:about.html.twig', array(
            //...
        ));
    }

    public function contactAction()
    {
        $enquiry = new Enquiry();
        $form = $this->createForm(new EnquiryType(), $enquiry);

        $request = $this->getRequest();
        $session = $request->getSession();
        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);
            if ($form->isValid()) {
                // Perform some action, such as sending an email
                $message = \Swift_Message::newInstance()
                ->setSubject('Contact Mail')
                ->setFrom('stevenjamesyung@icloud.com')
                ->setTo('stevenyung@icloud.com')
                ->setBody(
                $this->renderView(
                // src/Blogger/BlogBundle/Ressources/views/Page/registration.html.twig
                'BloggerBlogBundle:Page:contactEmail.html.twig',
                array(
                    'enquiry'   => $enquiry,
                )
            ),
            'text/html'
            )
            ;
            $this->get('mailer')->send($message);
            $this->addFlash('notice', 'Your contact enquiry was successfully sent. Thank you!');

            // Redirect - This is important to prevent users re-posting
            // the form if they refresh the page
            return $this->redirect($this->generateUrl('BloggerBlog_contact'));
        }
    }
    return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
        'form' => $form->createView()
    ));
    //     return $this->render('BloggerBlogBundle:Page:contact.html.twig', array(
    //     //...
    // ));
}
}
